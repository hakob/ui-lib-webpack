import React from 'react';
import { createUseStyles } from 'react-jss'

const useStyles = createUseStyles({
    myButton: {
        color: 'yellow',
        margin: {
            top: 5,
            right: 0,
            bottom: 0,
            left: '1rem'
        },
        '& span': {
            fontWeight: 'bold'
        }
    },
    myLabel: {
        fontStyle: 'italic'
    }
})

const Button = (props : any) => {
    const classes = useStyles()
    return (
      <div>
          <button className={classes.myButton}>
              {props?.children}
          </button>
      </div>
    )
}

export default Button
