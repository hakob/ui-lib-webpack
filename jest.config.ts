import type { Config } from '@jest/types';

const SRC_PATH: string = '<rootDir>/src';
const config: Config.InitialOptions = {
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  roots: [
    '<rootDir>/tests',
  ],
  setupFilesAfterEnv: ['<rootDir>/tests/setupTests.ts'],
  testMatch: ['<rootDir>/tests/**/?(*.)+(test).[jt]s?(x)'],
  modulePaths: [SRC_PATH],
  errorOnDeprecated: true,
  verbose: true,
};

export default config;
